<?php

ini_set('date.timezone', 'America/Sao_Paulo');

use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use FOS\UserBundle\FOSUserBundle;
use Silex\Provider\TranslationServiceProvider;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Tobiassjosten\Silex\Provider\FacebookServiceProvider;
use Silex\Provider\SwiftmailerServiceProvider;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__.'/../vendor/autoload.php';

require_once __DIR__.'/../app/Classes/CapsuleServiceProvider.php';
require_once __DIR__.'/../app/Classes/twitteroauth/twitteroauth.php';
require_once __DIR__.'/../app/Classes/MobileDetect.php';

require_once __DIR__.'/../app/Controllers/Api.php';

require_once __DIR__.'/../app/Controllers/Flight.php';
require_once __DIR__.'/../app/Controllers/Payment.php';
require_once __DIR__.'/../app/Controllers/Confirmation.php';
require_once __DIR__.'/../app/Controllers/Hotel.php';
require_once __DIR__.'/../app/Controllers/Car.php';
require_once __DIR__.'/../app/Controllers/Deals.php';
require_once __DIR__.'/../app/Controllers/User.php';
require_once __DIR__.'/../app/Controllers/Corporate.php';
require_once __DIR__.'/../app/Controllers/Contact.php';
require_once __DIR__.'/../app/Controllers/Print.php';
require_once __DIR__.'/../app/Controllers/Share.php';
require_once __DIR__.'/../app/Models/User.php';
require_once __DIR__.'/../app/Models/Region.php';
require_once __DIR__.'/../app/Models/Airport.php';

$app = new Silex\Application();
$detect = new Mobile_Detect;

$app['config'] = require('../app/Config/application.php');
$app['debug'] = true;

$app['isMobile'] = $detect->isMobile();
$app['userIP'] = getenv('HTTP_CLIENT_IP')?:getenv('HTTP_X_FORWARDED_FOR')?:getenv('HTTP_X_FORWARDED')?:getenv('HTTP_FORWARDED_FOR')?:getenv('HTTP_FORWARDED')?:getenv('REMOTE_ADDR');

$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../app/Views',
));

$app->register(new UrlGeneratorServiceProvider());
$app->register(new TranslationServiceProvider(), array(
    'locale_fallbacks' => array('en'),
));
$app->register(new SessionServiceProvider());
$app->register(new ValidatorServiceProvider());

$app['translator'] = $app->share($app->extend('translator', function($translator, $app) {
    $translator->addLoader('yaml', new YamlFileLoader());
    $translator->addResource('yaml', __DIR__.'/../app/Locales/de.yml', 'de');
    $translator->addResource('yaml', __DIR__.'/../app/Locales/en.yml', 'en');
    $translator->addResource('yaml', __DIR__.'/../app/Locales/es.yml', 'es');
    $translator->addResource('yaml', __DIR__.'/../app/Locales/fr.yml', 'fr');
    $translator->addResource('yaml', __DIR__.'/../app/Locales/it.yml', 'it');
    $translator->addResource('yaml', __DIR__.'/../app/Locales/pt_br.yml', 'pt_br');

    return $translator;
}));


$app->register(new CapsuleServiceProvider, array(

    // DB Connection: Single.
    'capsule.connection' => array(
        'driver'    => 'mysql',
        'host'      => $app['config']['db.host'],
        'database'  => $app['config']['db.database'],
        'username'  => $app['config']['db.username'],
        'password'  => $app['config']['db.password'],
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    )

));

$transport = Swift_SmtpTransport::newInstance('mail.hashtagtravels.com', 26)
    ->setUsername('noreply@hashtagtravels.com')
    ->setPassword('#travels@12');

$app->register(new SwiftmailerServiceProvider());
$app['swiftmailer.transport'] = $transport;

// $app['swiftmailer.options'] = array(
//     'host' => 'mail.hashtagtravels.com',
//     'port' => '26',
//     'username' => 'noreply@hashtagtravels.com',
//     'password' => '#travels@12',
//     'encryption' => null,
//     'auth_mode' => null
// );



// $app->get('/test', function() use ($app) {

//     return $app['twig']->render('email/reset-password.twig', array(
//         'name' => 'Pedro',
//         'key' => 'abasdas'
//     ));

//     $message = \Swift_Message::newInstance()
//         ->setSubject('[YourSite] Feedback')
//         ->setFrom(array('dev@hashtagtravels.com'))
//         ->setTo(array('eu@guilhermemedeiros.com.br'))
//         ->setBody($app['twig']->render('email/reset-password.twig', array(
//             'name' => 'Pedro',
//             'key' => 'abasdas'
//         )), 'text/html');

//     $app['mailer']->send($message);

//     // return '';

// });


$app->register(new FacebookServiceProvider(), array(
    'facebook.app_id'     => $app['config']['facebook.app_id'],
    'facebook.secret'     => $app['config']['facebook.secret']
));

$app['locale'] = $app['session']->get('current_language') ? $app['session']->get('current_language') : 'en';
$app['currency'] = $app['session']->get('current_currency') ? $app['session']->get('current_currency') : array('symbol' => 'CHF', 'name' => 'Swiss Franc');
$app['user'] = $app['session']->get('user');


$app->get('/api/lastTweets', 'Controllers\\Api::lastTweets');

$app->get('/', 'Controllers\\Flight::index')->bind('home');
$app->get('/flight/flex', 'Controllers\\Flight::flex')->bind('flight_flex');
$app->get('/flight/search', 'Controllers\\Flight::search')->bind('flight_search');
$app->post('/flight/search', 'Controllers\\Flight::format_search')->bind('flight_search_post');
$app->get('/flight/checkout', 'Controllers\\Flight::checkout')->bind('flight_checkout');
$app->post('/flight/checkout', 'Controllers\\Flight::checkout_payment')->bind('flight_checkout_payment');

$app->post('/payment', 'Controllers\\Payment::callback')->bind('payment_callback');
$app->post('/payment/sign', 'Controllers\\Payment::getSign')->bind('payment_sign');


// flight + hotel packages
$app->get('/flight/hotel-package', 'Controllers\\Flight::hotel_search')->bind('flight_hotel_search');
$app->get('/flight/flight-package', 'Controllers\\Flight::flight_search')->bind('flight_search_hotel');


$app->get('/hotel', 'Controllers\\Hotel::index')->bind('hotel');
$app->get('/hotel/search', 'Controllers\\Hotel::search')->bind('hotel_search');
$app->get('/hotel/detail', 'Controllers\\Hotel::detail')->bind('hotel_detail');
$app->get('/hotel/checkout', 'Controllers\\Hotel::checkout')->bind('hotel_checkout');
$app->post('/hotel/checkout', 'Controllers\\Hotel::checkout_payment')->bind('hotel_checkout_payment');

$app->get('/car', 'Controllers\\Car::index')->bind('car');
$app->get('/car/search', 'Controllers\\Car::search')->bind('car_search');
$app->get('/car/checkout', 'Controllers\\Car::checkout')->bind('car_checkout');

$app->get('/deals', 'Controllers\\Deals::index')->bind('deals');
$app->get('/deals/search', 'Controllers\\Deals::search')->bind('deals_search');
$app->get('/deals/hotel', 'Controllers\\Deals::hotel')->bind('deals_hotel');
$app->get('/deals/flight', 'Controllers\\Deals::flight')->bind('deals_flight');
$app->get('/deals/checkout', 'Controllers\\Deals::checkout')->bind('deals_checkout');

$app->get('/user/', 'Controllers\\User::my_account')->bind('user_home');

$app->get('/about-us', 'Controllers\\Corporate::about_us')->bind('about_us');
$app->get('/hashtag-rewards', 'Controllers\\Corporate::hashtag_rewards')->bind('hashtag_rewards');
$app->get('/terms-and-conditions', 'Controllers\\Corporate::terms')->bind('terms');
$app->get('/privacy-policy', 'Controllers\\Corporate::privacy_policy')->bind('privacy_policy');

$app->get('/contact', 'Controllers\\Contact::contact')->bind('contact');
$app->post('/contact', 'Controllers\\Contact::send')->bind('contact_send');

$app->get('/user/bookings', 'Controllers\\User::bookings')->bind('user_bookings');
$app->get('/user/personal_details', 'Controllers\\User::personal_details')->bind('user_personal_details');
$app->get('/user/settings', 'Controllers\\User::settings')->bind('user_settings');
$app->get('/user/rewards', 'Controllers\\User::rewards')->bind('user_rewards');
$app->get('/user/notifications', 'Controllers\\User::notifications')->bind('user_notifications');

$app->get('/user/personal_details.json', 'Controllers\\User::get_personal_details')->bind('user_get_personal_details');
$app->put('/user/personal_details.json', 'Controllers\\User::put_personal_details')->bind('user_put_personal_details');
$app->put('/user/settings.json', 'Controllers\\User::put_settings')->bind('user_put_settings');
$app->get('/user/notifications.json', 'Controllers\\User::get_notifications')->bind('user_get_notifications');
$app->put('/user/notifications.json', 'Controllers\\User::put_notifications')->bind('user_put_notifications');

// $app->get('/user/my_account/', 'Controllers\\User::my_account')->bind('user_my_account');

$app->get('/signin', 'Controllers\\User::signin')->bind('user_signin');
$app->get('/logout', 'Controllers\\User::logout')->bind('user_logout');
$app->delete('/delete_account', 'Controllers\\User::delete_account')->bind('user_delete_account');
$app->post('/signin', 'Controllers\\User::post_signin')->bind('post_user_signin');
$app->get('/signin/twitter', 'Controllers\\User::signin_twitter')->bind('user_signin_twitter');
$app->post('/signup', 'Controllers\\User::post_signup')->bind('post_user_signup');

$app->post('/forgot_password', 'Controllers\\User::post_forgot_password')->bind('post_user_forgot_password');
$app->get('/forgot_password/{key}', 'Controllers\\User::get_forgot_password')->bind('get_user_forgot_password');

$app->get('/share/hotel', 'Controllers\\Share::hotel');
$app->get('/share/car', 'Controllers\\Share::car');


// Print - flight
$app->get('/print/flight', 'Controllers\\PrintPage::flight')->bind('print_flight');
$app->get('/print/flight/confirmation', 'Controllers\\PrintPage::flightConfirmation')->bind('print_flight_confirmation');

$app->get('/print/hotel', 'Controllers\\PrintPage::hotel')->bind('print_hotel');
$app->get('/print/hotel/confirmation', 'Controllers\\PrintPage::hotelConfirmation')->bind('print_hotel_confirmation');

$app->get('/print/car', 'Controllers\\PrintPage::car')->bind('print_car');
$app->get('/print/car/confirmation', 'Controllers\\PrintPage::carConfirmation')->bind('print_car_confirmation');

$app->get('/print/deals', 'Controllers\\PrintPage::deals')->bind('print_deals');
$app->get('/print/deals/confirmation', 'Controllers\\PrintPage::dealsConfirmation')->bind('print_deals_confirmation');

$app->get('/lang/{lang}', function($lang) use($app) {
    $app['session']->set('current_language', $lang);

    return $app->redirect($_SERVER['HTTP_REFERER']);
})->bind('change_language');

$app->get('/directive/{name}', function ($name) use ($app) {
    return $app['twig']->render('directive/'.$name.'.twig');
});

$app->get('/currency/{currency}', function($currency) use($app) {

    $currencies = array(
        "AED" => "U.A.E. Dirham",
        "ALL" => "Albanian Lek",
        "ARS" => "Argentine Peso",
        "AUD" => "Australian Dollar",
        "BGN" => "Bulgarian Lev",
        "BOB" => "Bolivian Boliviano ",
        "BRL" => "Brazilian Real",
        "CAD" => "Canadian Dollar",
        "CHF" => "Swiss Franc",
        "CLP" => "Chilean Peso",
        "CNY" => "Yuan Renminbi",
        "COP" => "Colombian Peso",
        "CZK" => "Czech Koruna",
        "DKK" => "Danish Krone",
        "EEK" => "Kroon",
        "EUR" => "Euro",
        "GBP" => "Pound Sterling",
        "HKD" => "Hong Kong Dollar",
        "HRK" => "Croatian Kuna",
        "HUF" => "Forint",
        "ILS" => "New Israeli Sheqel",
        "INR" => "Indian Rupee",
        "ISK" => "Icelandic Krona",
        "JPY" => "Yen",
        "KZT" => "Kazakhstan Tenge",
        "KRW" => "Won",
        "LKR" => "Sri Lanka Rupee",
        "LTL" => "Lithuanian Litas",
        "LVL" => "Latvian Lats",
        "MAD" => "Moroccan Dirham",
        "MXN" => "Mexican Peso",
        "MYR" => "Malaysian Ringgits",
        "NOK" => "Norwegian Kroner",
        "NPR" => "Nepalese Rupee",
        "NZD" => "New Zealand Dollar",
        "PEN" => "Peruvian Peso",
        "PKR" => "Pakistan Rupee",
        "PLN" => "Zloty",
        "PYG" => "Paraguayan Guarani",
        "PHP" => "Philippine Peso",
        "RON" => "Leu",
        "RSD" => "Serbian Dinar",
        "RUB" => "Russian Ruble",
        "SAR" => "Saudi Riyal",
        "SEK" => "Swedish Krona",
        "SGD" => "Singapore Dollars",
        "THB" => "Bhart",
        "TRY" => "New Turkish Lira",
        "TWD" => "New Taiwan Dollar",
        "UAH" => "Ukrainian Hryvnia",
        "USD" => "U.S. Dollar",
        "UYU" => "Uruguayan Peso",
        "UZS" => "Uzbekistan Som",
        "VEF" => "Venezuelan Bolivar",
        "ZAR" => "Rand"
    );


    $app['session']->set('current_currency', array(
        'symbol' => $currency,
        'name' => $currencies[$currency]
    ));

    return $app->redirect($_SERVER['HTTP_REFERER']);
})->bind('change_currency');

$stack = (new Stack\Builder())
    ->push('Atst\StackBackstage', __DIR__.'/maintenance.html');

$app = $stack->resolve($app);

$request = Request::createFromGlobals();
$response = $app->handle($request)->send();
$app->terminate($request, $response);

// $app->run();

