//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('DealsGrid', ['DealsGrid.Controllers', 'DealsGrid.Services', 'DealsGrid.Directives'])
	.config(function($interpolateProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})
	.run(function($rootScope, DealsService){
		$rootScope.modal = false;

		$rootScope.closeModal = function(e){
			e.preventDefault();
			$rootScope.modal = false;
		}
	});

//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('DealsGrid.Controllers', [])

	.controller('results', function($scope, DealsService, $q, $rootScope){
		DealsService.getResults().then(function (data) {
			$scope.results = data;
			$scope.limit = 15;
		});
	});

//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('DealsGrid.Services', [])

	.service('DealsService', function($http, $q){

		var getResultsPromise;
		this.getResults = function(search){
			if(getResultsPromise) return getResultsPromise;

			var defer = $q.defer();

			$http.get(App.uploadsDir + '/deals-grid.json').then(function(response){
				defer.resolve(response.data);
			})

			return getResultsPromise = defer.promise;
		}
	});

//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('DealsGrid.Directives', [])

	.directive('assistOverflow', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs) {
				$(elm).on('scroll', function(e){
					if($(this).scrollTop() + 400 > Math.abs($(this).height() - $(this).get(0).scrollHeight)){
						scope.$apply(function(){
							scope.limit += 5;
						});
					}

				})
			}
		}
	})

	.directive('masonry', function () {
			return {
				restrict: 'A',
				link: function(scope, elm, attrs) {

					var $container = $(elm);

					scope.$watch('results', function(data){
						if(!data) return;

						setTimeout(function(){
							var __resetLayout = Packery.prototype._resetLayout;
							Packery.prototype._resetLayout = function() {
								__resetLayout.call( this );
								var parentSize = getSize( this.element.parentNode );
								var colW = this.columnWidth + this.gutter;
								this.fitWidth = Math.floor( ( parentSize.innerWidth + this.gutter ) / colW ) * colW;
								this.packer.width = this.fitWidth;
								this.packer.height = Number.POSITIVE_INFINITY;
								this.packer.reset();
							};

							Packery.prototype._getContainerSize = function() {
								var emptyWidth = 0;
								for ( var i=0, len = this.packer.spaces.length; i < len; i++ ) {
									var space = this.packer.spaces[i];
									if ( space.y === 0 && space.height === Number.POSITIVE_INFINITY ) {
										emptyWidth += space.width;
									}
								}

								return {
									width: this.fitWidth - this.gutter,
									height: this.maxY - this.gutter
								};
							};

							Packery.prototype.resize = function() {
								this.layout();
							};

							$container.packery({
								columnHeight : 200,
								columnWidth: 240,
								itemSelector : '.deals-grid__box',
								isFitWidth : true,
								isAnimated : false,
								transitionDuration: 0
							});
							$container.addClass('is--loaded');
						}, 200);

					})

					scope.$watch('limit', function(newlimit, previouslimit){
						if(!newlimit || !previouslimit) return;

						setTimeout(function(){
							$container.packery('reloadItems')
							$container.packery('layout')
						})

					})


				}
			}
	});
