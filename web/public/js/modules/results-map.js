App.Modules['results-map'] = (function(){

	'use strict';

	function MVCArrayBinder(mvcArray){
		this.array_ = mvcArray;
	}
	MVCArrayBinder.prototype = new google.maps.MVCObject();
	MVCArrayBinder.prototype.get = function(key) {
		if (!isNaN(parseInt(key))){
			return this.array_.getAt(parseInt(key));
		} else {
			this.array_.get(key);
		}
	}
	MVCArrayBinder.prototype.set = function(key, val) {
		if (!isNaN(parseInt(key))){
			this.array_.setAt(parseInt(key), val);
		} else {
			this.array_.set(key, val);
		}
	}

	/****/

	function Label(opt_options) {
		this.setValues(opt_options);
		var span = this.span_ = document.createElement("span");
		span.className = 'label-title';
		var div = this.div_ = document.createElement("div");
		div.className = "airportPinLabel"
		div.appendChild(span);
		div.style.cssText = "position: absolute; display: none"
	}

	Label.prototype = new google.maps.OverlayView;

	Label.prototype.onAdd = function () {
		var pane = this.getPanes().overlayImage;
		pane.appendChild(this.div_);
		var me = this;
		this.listeners_ = [google.maps.event.addListener(this, "position_changed", function () {
				me.draw()
			}), google.maps.event.addListener(this, "text_changed", function () {
				me.draw()
			}), google.maps.event.addListener(this, "zindex_changed", function () {
				me.draw()
			})]
	};

	Label.prototype.onRemove = function () {
		this.div_.parentNode.removeChild(this.div_);
		for (var i = 0, I = this.listeners_.length; i < I; ++i) {
			google.maps.event.removeListener(this.listeners_[i])
		}
	};

	Label.prototype.draw = function () {
		var projection = this.getProjection();
		var position = projection.fromLatLngToDivPixel(this.get("position"));
		var div = this.div_;
		div.style.left = position.x + "px";
		div.style.top = position.y + "px";
		div.style.display = "block";
		div.style.fontFamily = "Asap";
		div.style.fontWeight = "bold";
		div.style.fontSize = '20px';

		div.style.zIndex = this.get("zIndex");
		this.span_.innerHTML = this.get("text").toString()
	};



	/***/

	// var mapStyle = [{"featureType":"administrative","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"simplified"}]},{"featureType":"road","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"color":"#5f94ff"},{"lightness":26},{"gamma":5.86}]},{},{"featureType":"road.highway","stylers":[{"weight":0.6},{"saturation":-85},{"lightness":61}]},{"featureType":"road"},{},{"featureType":"landscape","stylers":[{"hue":"#0066ff"},{"saturation":74},{"lightness":100}]}];
	// var mapStyle = [{"featureType":"water","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"landscape","stylers":[{"color":"#f2f2f2"}]},{"featureType":"road","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]}];
	// var mapStyle = [{"featureType":"water","stylers":[{"color":"#19a0d8"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"weight":6}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#e85113"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efe9e4"},{"lightness":-40}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#efe9e4"},{"lightness":-20}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"lightness":100}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"lightness":-100}]},{"featureType":"road.highway","elementType":"labels.icon"},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape","stylers":[{"lightness":20},{"color":"#efe9e4"}]},{"featureType":"landscape.man_made","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"lightness":100}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"lightness":-100}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"hue":"#11ff00"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"lightness":100}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"hue":"#4cff00"},{"saturation":58}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#f0e4d3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#efe9e4"},{"lightness":-25}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#efe9e4"},{"lightness":-10}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"simplified"}]}];
	var mapStyle = [{"stylers":[{"visibility":"off"}]},{"featureType":"road","stylers":[{"visibility":"on"},{"color":"#ffffff"}]},{"featureType":"road.arterial","stylers":[{"visibility":"on"},{"color":"#fee379"}]},{"featureType":"road.highway","stylers":[{"visibility":"on"},{"color":"#fee379"}]},{"featureType":"landscape","stylers":[{"visibility":"on"},{"color":"#f3f4f4"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"color":"#7fc8ed"}]},{},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#83cead"}]},{"elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"weight":0.9},{"visibility":"off"}]}];

	var mapOptions = {
		zoom: 3,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		styles: mapStyle
	}


	var map;

	var airportsMarkers = [];
	var image = App.staticPath + '/img/modules/deal/city.png';
	var airportMarkerImage = new google.maps.MarkerImage(image,
		new google.maps.Size(12, 12), //size
		new google.maps.Point(0, 0), //origin point
		new google.maps.Point(7, 6)); // offset point


	var path;


	function angularLink(scope, elm, attrs){
		map = new google.maps.Map(elm[0], mapOptions);
		path = new google.maps.Polyline({
			map: map,
			geodesic: true,
			strokeOpacity: 0,
			strokeColor: '#666666',
			zIndex: 3,
			icons: [
				{
					icon: {
						path: 'M 0,-1 0,1',
						strokeColor: 'black',
						strokeWeight: 1,
						strokeOpacity: .5,
					},
					repeat: '15px',
				},


				// {
				// 	icon: {
				// 		path: google.maps.SymbolPath.CIRCLE,
				// 		scale: 5,
				// 		strokeColor: '#333',
				// 		strokeOpacity: 1

				// 	 },

				// 	offset: '100%'
				// }

				{
					icon: {
						// path: 'm 124.13215,203.94801 c 6.51836,-40.19499 7.83356,-86.39235 7.83356,-86.39235 L 228.366,168.95619 224.19242,150.71629 132.4769,73.129497 C 129.62092,20.872171 123.10255,5.2338531 123.10255,5.2338531 120.47217,-0.25491786 114.2394,0.00188014 114.2394,0.00188014 h -0.0552 -0.0552 c 0,0 -6.23517,-0.256798 -8.86315,5.23197296 0,0 -6.518368,15.6383179 -9.374353,67.8956439 L 4.1735775,150.71869 -4.749632e-7,168.95619 96.400294,117.55566 c 0,0 1.315193,46.19736 7.833556,86.39235 0,0 -28.759046,26.13106 -28.216649,27.95985 v 7.31756 c 0,0 1.286394,1.3176 5.74797,0.516 l 25.043869,-8.60635 c 0,0 4.74717,-1.056 7.37516,-0.7704 2.63039,-0.2856 7.37516,0.7704 7.37516,0.7704 l 25.04387,8.60635 c 4.45918,0.8016 5.74797,-0.516 5.74797,-0.516 v -7.31516 c 0.54,-1.83119 -28.21905,-27.96225 -28.21905,-27.96225 z',
						// path: 'M362.985,430.724l-10.248,51.234l62.332,57.969l-3.293,26.145 l-71.345-23.599l-2.001,13.069l-2.057-13.529l-71.278,22.928l-5.762-23.984l64.097-59.271l-8.913-51.359l0.858-114.43 l-21.945-11.338l-189.358,88.76l-1.18-32.262l213.344-180.08l0.875-107.436l7.973-32.005l7.642-12.054l7.377-3.958l9.238,3.65 l6.367,14.925l7.369,30.363v106.375l211.592,182.082l-1.496,32.247l-188.479-90.61l-21.616,10.087l-0.094,115.684',
						// path: google.maps.SymbolPath.CIRCLE,
						path: 'M56.71,37.979L35.672,26.232c-0.471-0.26-0.867-0.912-0.88-1.449L34.218,9.05c-0.054-1.618-1.123-2.617-2.217-2.617 c-1.095,0-2.162,0.999-2.22,2.617L29.21,24.785c-0.016,0.537-0.407,1.188-0.88,1.45L7.291,37.979 c-0.471,0.26-0.856,0.913-0.856,1.451v3.684c0,0.535,0.42,0.85,0.937,0.694l20.613-6.272c0.516-0.155,0.935,0.156,0.935,0.695 v10.117c0,0.538-0.337,1.262-0.748,1.608l-3.541,2.976c-0.41,0.348-0.748,1.071-0.748,1.607v2.264c0,0.537,0.425,0.866,0.946,0.733 l7.172-1.775l7.169,1.775c0.521,0.133,0.947-0.196,0.947-0.733V54.54c0-0.536-0.338-1.26-0.75-1.607l-3.541-2.976 c-0.409-0.347-0.747-1.07-0.747-1.608V38.23c0-0.538,0.421-0.851,0.934-0.695l20.614,6.272c0.517,0.155,0.938-0.159,0.938-0.694 V39.43C57.565,38.891,57.18,38.238,56.71,37.979z',
						scale: 0.4,

						strokeOpacity: 1,
						fill: 'black',
						fillColor: "#black",
						fillOpacity: 1,
						strokeWeight: 1,
						strokeColor: 'black'
					},

					offset: '100%'
				}
			]
		});
	}

	function update(routes, airports){

		if(!map) return;

		var markersToReuse = [];
		var markersAlreadyOnMap = {}


		//Mark current airport markers to be reused
		_.each(airportsMarkers, function(marker, index){

			if(_.indexOf(routes, marker.airportCode) == -1){
				// marker.setMap(null);
				markersToReuse.push(marker);
			} else {
				markersAlreadyOnMap[marker.airportCode] = marker;
			}
		})

		airportsMarkers = []

		_.each(routes, function(airportCode){
			if(!airports[airportCode]) return;

			if(!markersAlreadyOnMap[airportCode]){

				if(markersToReuse.length){

					var marker = markersToReuse[0];
						marker.airportCode = airportCode;
						marker.setPosition(new google.maps.LatLng(airports[airportCode].lat, airports[airportCode].lon))
						marker.label.setMap(null);
						marker.label = null;

					markersToReuse.splice(0, 1);

					airportsMarkers.push(marker)

				} else {
					airportsMarkers.push(
						new google.maps.Marker({
							position: new google.maps.LatLng(airports[airportCode].lat, airports[airportCode].lon),
							map: map,
							icon: airportMarkerImage,
							shadow: false,
							zIndex: 2,
							animation: google.maps.Animation.FADE,
							airportCode: airportCode
						})
					)
				}
			} else {
				airportsMarkers.push(markersAlreadyOnMap[airportCode]);
			}
		})


		_.each(airportsMarkers, function(marker){
			if(!marker.label){

				marker.label = new Label({ map: map });
				marker.label.set("zIndex", 2);
				marker.label.set("position", marker.getPosition());
				// marker.label.bindTo("position", marker, "position");
				marker.label.set("text", marker.airportCode);
			}

		})

		//Clean unused markers
		_.each(markersToReuse, function(marker){
			marker.label.setMap(null);
			marker.setMap(null);
		})

		fixPaths()
	}


	function fixPaths() {
		var latLngs = [];
		var bounds = new google.maps.LatLngBounds();

		airportsMarkers.forEach(function(marker, index){
			var position = marker.getPosition();
			latLngs.push(position)
			bounds.extend(position);
		})

		path.setPath(latLngs)
		path.binder = new MVCArrayBinder(path.getPath());
		map.fitBounds(bounds);

		airportsMarkers.forEach(function(marker, index){
			marker.bindTo('position', path.binder, index.toString());
		})

		animateRoute();

	}

	var interval;
	function animateRoute() {
		clearInterval(interval);
		var count = 0;
		interval = setInterval(function() {
			count = (count + 1) % 400;
			var icons = path.get('icons');
			icons[0].offset = (count / 6) + '%';
			icons[1].offset = (count / 4) + '%';

			path.set('icons', icons);
		}, 20);
	}


	function run(){

		/**
		 * Airports
		 */




		/**
		 * Destinations
		 */

		// var markers = [];

		// function addMarker(latLng){
		// 	var prevLatLng;
		// 	var image = App.staticPath + '/img/modules/deal/marker.png';
		// 	var marker = new google.maps.Marker({
		// 		position: latLng,
		// 		map: map,
		// 		icon: image,
		// 		draggable: true
		// 	});

		// 	markers.push(marker)
		// 	var index = markers.length - 1;

		// 	google.maps.event.addListener(marker, "dragend", function(e) {
		// 		var lat = e.latLng.lat()
		// 		var lng = e.latLng.lng()

		// 		var closeAirportLatLng;

		// 		for (var airport in airports) {
		// 			if(Math.sqrt(Math.pow(lat - airports[airport].center.lat(), 2) + Math.pow(lng - airports[airport].center.lng(), 2)) < 1.5) {
		// 				closeAirportLatLng = airports[airport].center;
		// 				break;
		// 			}
		// 		}

		// 		var newLatLng = closeAirportLatLng || prevLatLng;

		// 		path.binder.set(index.toString(), newLatLng);
		// 		marker.bindTo('position', path.binder, index.toString());


		// 		setArea(markers[0].getPosition());

		// 	});

		// 	google.maps.event.addListener(marker, "dragstart", function(e) {
		// 		prevLatLng = this.getPosition();
		// 	});

		// }





		// addMarker(airports['london'].center);
		// addMarker(airports['geneva'].center);
		// addMarker(airports['berlim'].center);


		// animateRoute();


	}

	return {
		angularLink: angularLink,
		update: update,
		run: run
	}

}());