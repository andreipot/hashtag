//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('HotelDetailApp', ['HotelDetailApp.Controllers', 'HotelDetailApp.Services', 'HotelDetailApp.Directives'])
	.config(function($interpolateProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})



//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('HotelDetailApp.Controllers', [])
	.controller('detail', function($scope, HotelDetailService) {

		HotelDetailService.getDetail().then(function(response) {
			$scope.hotel = response;
		})

	})




//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('HotelDetailApp.Services', [])

	.service('HotelDetailService', function($http, $q) {

		var getDetailPromise;
		this.getDetail = function(){

			if(getDetailPromise) return getDetailPromise;

			var defer = $q.defer();

			$http.get(App.uploadsDir + '/hotel-detail.json').then(function(response){
				defer.resolve(response.data);
			});

			return getDetailPromise = defer.promise;
		}

	})




//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('HotelDetailApp.Directives', [])

	.directive('onLastRepeat', function() {
		return function(scope, element, attrs) {
			if (scope.$last) setTimeout(function(){
				App.Modules['carrousel'].run();
			}, 1);
		};
	})
