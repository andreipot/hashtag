//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('HotelService', [])

	.service('HotelService', function($http, $q, $timeout, $rootScope, localStorageService, TEST){

		var getResultsPromise;
		this.getResults = function(search){
			if(getResultsPromise) return getResultsPromise;

			var defer = $q.defer();

			search = search.replace(/(&?\w+=((?=$)|(?=&)))/g,'');
			
			$http.get((TEST) ? App.uploadsDir + '/hotel.json' : App.baseAPIUrl + '/hotel/search' + search + '&currency=' + App.selectedCurrency, {withCredentials: true})
				.then(function(response){
					defer.resolve(response.data)
				})

			return getResultsPromise = defer.promise;
		};

		var getGuestsPromise;
		this.getGuests = function(){
			if(getGuestsPromise) return getGuestsPromise;

			var defer = $q.defer();
			var guests = localStorageService.get('guests');

			defer.resolve(guests);

			return getGuestsPromise = defer.promise;
		}

		var getPricingPromise;
		this.getPricing = function(){
			if(getPricingPromise) return getPricingPromise;

			var defer = $q.defer();

			var selectedRoom = localStorageService.get('selectedRoom');
			var guests = localStorageService.get('guests');

			var data = {}

			data.selectedRoom = {}
			data.selectedRoom.providerId = selectedRoom.providerId
			data.selectedRoom.currency = selectedRoom.currency
			data.selectedRoom.hotelId = selectedRoom.hotelId
			data.selectedRoom.roomXMLQuoteID = selectedRoom.roomXMLQuoteID
			data.selectedRoom.hotelsProProcessId = selectedRoom.hotelsProProcessId
			data.selectedRoom.checkin = selectedRoom.checkin
			data.selectedRoom.checkout = selectedRoom.checkout
			data.selectedRoom.totalPrice = selectedRoom.totalPrice
			data.selectedRoom.searchID = selectedRoom.searchID

			data.guests = guests

			$http.post(App.baseAPIUrl + '/hotel/pricing', data)
				.then(function(response){
					defer.resolve(response.data);
				})

			return getPricingPromise = defer.promise;
		}

		this.roomsAndGuests = function(search){
			var query_object = this.queryToObject(search);
			var adults = query_object.adults;
			var childs = query_object.childs;
			var rooms = query_object.rooms;
		}

		this.queryToObject = function(search){
			var parms = {};
			var temp;
			var items = search.slice(1).split("&");   // remove leading ? and split
			for (var i = 0; i < items.length; i++) {
				temp = items[i].split("=");
				if (temp[0]) {
					if (temp.length < 2) {
						temp.push("");
					}
					parms[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);        
				}
			}

			return parms;
		}

		this.setPersonalDetails = function(data){
			return $http.post(App.baseAPIUrl + '/hotel/paymentDetails', data, {withCredentials: true});
		}

		var getPaymentDetailsPromise;
		this.getPaymentDetails = function(){
			if(getPaymentDetailsPromise) return getPaymentDetailsPromise;

			var defer = $q.defer();

			defer.resolve(localStorageService.get('hotelPaymentDetails')); //just to share with othe controllers

			return getPaymentDetailsPromise = defer.promise;

		}

		var bookingPromise;
		this.booking = function(data){
			if(bookingPromise) return bookingPromise;

			var defer = $q.defer();

			$http.post(App.baseAPIUrl + '/hotel/book', data)
				.then(function(response){
					defer.resolve(response);
				})

			return bookingPromise = defer.promise;
		}

		this.filter = function(filters) {

			var defer = $q.defer();

			getResultsPromise.then(function(data){

				defer.resolve(_.filter(data.hotels, function(hotel){
					if(hotel.error) return false;

					var valid = true;

					// rooms
					if(hotel.startPrice < filters.price.from || hotel.startPrice > filters.price.to) {
						valid = false;
					}

					// stars
					if(valid && hotel.stars < filters.stars) {
						valid = false;
					}

					// facilities
					if(valid && filters.facilities.length) {
						var facilitiesValid = false;
						_.each(filters.facilities, function(facility){
							if(_.indexOf(hotel.facilities, facility.text) >= 0) {
								facilitiesValid = true;
							}
						});
						if(!facilitiesValid) valid = false;
					}

					return valid;
				}))

			});

			return defer.promise;
		}

		// var bookPromise;
		// this.book = function(){
		// 	if(bookPromise) return bookPromise;

		// 	var defer = $q.defer();

		// 	$http.post(App.baseAPIUrl + '/air/book', {"sendAnyData": 34}, {withCredentials: true}).then(function(response){
		// 		defer.resolve(response.data)
		// 	}, function(response){
		// 		defer.reject(response)
		// 	})

		// 	return bookPromise = defer.promise;
		// }

		// var ticketPromise;
		// this.ticket = function(datatrans_response){
		// 	if(ticketPromise) return ticketPromise;

		// 	var defer = $q.defer();

		// 	$http.post(App.baseAPIUrl + '/air/issue_ticket', datatrans_response, {withCredentials: true}).then(function(response){
		// 		defer.resolve(response.data)
		// 	}, function(response){
		// 		defer.reject(response)
		// 	})

		// 	return ticketPromise = defer.promise;
		// }

	})


	.service('HotelDetailService', function($http, $q, TEST) {

		this.getDetail = function(id, searchId, search, cacheId){

			var defer = $q.defer();

			search = search.replace(/(&?\w+=((?=$)|(?=&)))/g,'');

			$http.get((TEST) ? App.uploadsDir + '/hotel-detail.json' : App.baseAPIUrl + '/hotel/room/' + id + search + '&cacheId=' + cacheId + '&searchID=' + searchId + '&currency=' + App.selectedCurrency, {withCredentials: true})
				.then(function(response){
					defer.resolve(response.data)
				})

			return defer.promise;
		}


		this.getRoomPictures = function(id, searchId, search, cacheId) {

			var defer = $q.defer();

			// $http.get(App.baseAPIUrl + '/hotel/images?hotelId=' + id).then(function(response){
			$http.get((TEST) ? App.uploadsDir + '/hotel-pictures.json' : App.baseAPIUrl + '/hotel/room/' + id + search + '&cacheId=' + cacheId + '&searchID=' + searchId + '&currency=' + App.selectedCurrency, {withCredentials: true})
				.then(function(response){
					defer.resolve(response.data.pictures)
				})

			return defer.promise;
		}

	})

