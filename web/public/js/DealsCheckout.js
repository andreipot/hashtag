//     _                  ____                  _
//    / \   _ __  _ __   | __ )  ___   ___  ___| |_ _ __ __ _ _ __
//   / _ \ | '_ \| '_ \  |  _ \ / _ \ / _ \/ __| __| '__/ _` | '_ \
//  / ___ \| |_) | |_) | | |_) | (_) | (_) \__ \ |_| | | (_| | |_) |
// /_/   \_\ .__/| .__/  |____/ \___/ \___/|___/\__|_|  \__,_| .__/
//         |_|   |_|                                         |_|

angular.module('DealsCheckout', ['DealsCheckout.Controllers', 'DealsCheckout.Services', 'DealsCheckout.Directives', 'DealsCheckout.Filters'])
	.config(function($interpolateProvider){
		$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})
	.run(function($rootScope, State, FlightService){
		$rootScope.state = State.getCurrentState();

		$rootScope.backToPreviousState = function(e){
			if(State.getPreviousState()){
				e.preventDefault();
				State.backToPreviousState()
			}
		}

		$rootScope.modal = false;

		$rootScope.closeModal = function(e){
			e.preventDefault();
			$rootScope.modal = false;
		}

		State.onStateChange = function(){
			$rootScope.state = State.getCurrentState();
			$rootScope.previousState = State.getPreviousState();
		}

	})


//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

angular.module('DealsCheckout.Controllers', [])

	.controller('summary', function($scope, FlightService, $q, $rootScope){
		$q.all([FlightService.getDetail(), FlightService.getAirportsCode()]).then(function(responses){
			$scope.insurance = responses[0].insurance;
			$scope.flight = responses[0].flight;
			$scope.airports = responses[1];

			$scope.payment = responses[0].payment;
		})

		$scope.removeInsurance = function(e){
			e.preventDefault();
			FlightService.setInsurance(false);
			$rootScope.$emit('removeInsurance');
		}
	})

	.controller('personal-details', function($scope, FlightService, $q, $rootScope, State){
		$q.all([FlightService.getDetail()]).then(function(responses){
			$scope.passengers = responses[0].passengers;
		})

		$scope.submitPersonalDetails = function(e){
			e.preventDefault();
			if($scope.personalDetailsForm.$valid) State.nextState();
			return false;
		}
	})

	.controller('booking-options', function($scope, FlightService, $q, $rootScope, State){

		$q.all([FlightService.getDetail(), FlightService.getAirportsCode()]).then(function(responses){
			$scope.flight = responses[0].flight;
			$scope.airports = responses[1];
		})

		$rootScope.$on('removeInsurance', function(){
			$scope.selectedInsurance = false;
		});

		$scope.setInsurance = function(e, index){
			e.preventDefault();
			$scope.selectedInsurance = FlightService.setInsurance(index);
		}

		$scope.submitBookingOptions = function(e){
			e.preventDefault();
			if($scope.bookingOptionsForm.$valid) State.nextState();
			return false;
		}

		$scope.showManageLuggage = function(e){
			e.preventDefault();
			$rootScope.modal = 'manage-luggage';
		}

		$scope.showSelectSeat = function(e){
			e.preventDefault();
			$rootScope.modal = 'select-seat';
		}
	})

	.controller('payment-details', function($scope, FlightService, $q, $rootScope, State){

		$q.all([FlightService.getDetail()]).then(function(responses){
			$scope.payment = responses[0].payment = responses[0].payment || {};
		})

		$scope.submitPaymentDetails = function(e){
			e.preventDefault();
			console.log($scope.paymentDetailsForm)
			if($scope.paymentDetailsForm.$valid) State.nextState();
		}

		$scope.showCoupon = function(e){
			e.preventDefault();
			$rootScope.modal = 'coupon';
		}

	})

	.controller('coupon', function($scope, FlightService, $q, $rootScope, State){


	})

	.controller('manage-luggage', function($scope, FlightService, $q, $rootScope, State){
		$q.all([FlightService.getDetail()]).then(function(responses){
			$scope.passengers = responses[0].data.flight.passenger;

			console.log($scope.passengers)

			$scope.form = {
				selectedPassenger: $scope.passengers[0].key
			}
		})
	})

	.controller('select-seat', function($scope, FlightService, $q, $rootScope, State){

		$scope.currentFlightIndex = 0;
		$scope.currentPassengerIndex = 0;

		function generateZones(seat_lines){
			var seatHeight = 40;
			var currentCategory;
			var zones = [];
			var linesCategory = 1;
			var top = 0;
			_.each(seat_lines, function(line, row){
				var category = line[0].category;

				if(currentCategory !== category){
					zones.push({
						category: (currentCategory === undefined) ? category : currentCategory,
						height: linesCategory * seatHeight,
						top: top,
						price: line[0].price
					})

					top += linesCategory * seatHeight;
					linesCategory = 1;
				} else {
					linesCategory++;
				}
				currentCategory = category;
			});

			return zones;
		};

		function updateAirplane(){
			$scope.zones = generateZones($scope.flights[$scope.currentFlightIndex].seats);
			$scope.airplane = $scope.flights[$scope.currentFlightIndex].seats;
		}

		$scope.setCurrentFlight = function(e){
			e.preventDefault();
			$scope.currentFlightIndex = this.i;
			updateAirplane();

		}

		$scope.setCurrentPassenger = function(e){
			e.preventDefault();
			$scope.currentPassengerIndex = this.j;
		}

		$scope.setPassengerSeat = function(e){
			e.preventDefault();
			$scope.passengers[$scope.currentPassengerIndex]['selectedSeat'][$scope.currentFlightIndex] = this.row + this.seat.letter;
		}

		$scope.getSeatStatus = function(){
			var status = this.seat.status;
			var seatCode = parseInt(this.row, 10) + this.seat.letter;
			var name = '';

			_.each($scope.passengers, function(passenger) {
				if(passenger.selectedSeat[$scope.currentFlightIndex] == seatCode){
					status = 'selected selected-friend';
					name = passenger.name;
				}
			})

			if($scope.passengers[$scope.currentPassengerIndex].selectedSeat[$scope.currentFlightIndex] == seatCode) {
				status = 'selected';
			}

			this.name = name; //bad

			return status;
		}

		$q.all([FlightService.getSeatSelect(), FlightService.getAirportsCode()]).then(function(responses){
			$scope.flights = responses[0].data.flights;
			$scope.passengers = responses[0].data.passengers;

			_.each($scope.passengers, function(passenger){
				passenger.selectedSeat = {};
			})

			updateAirplane();
		})

	})


	.controller('confirmation', function($scope, FlightService, $q, $rootScope, State){
		FlightService.book().then(function(response){
			if(response.data.PNRViewRS) {
				$scope.PNR = response.data.PNRViewRS.PNRIdentification.RecordLocator;
			} else {
				alert('Error trying to get PNR')
			}

		})
	})


//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

angular.module('DealsCheckout.Services', [])
	.service('State', function(){
		var currentStateIndex = 0;
		var states = [
			{name: 'personal-details', string: 'Personal Details'},
			{name: 'booking-options', string: 'Booking Options'},
			{name: 'payment-details', string: 'Payment Details'},
			{name: 'confirmation', string: 'Confirmation'},
		]

		this.setState = function(index){
			currentStateIndex = index;
			this.onStateChange(index);
		}

		this.nextState = function(){
			this.setState(currentStateIndex+1);
		}

		this.backToPreviousState = function(){
			this.setState(currentStateIndex-1);
		}

		this.getPreviousState = function(){
			return states[currentStateIndex-1];
		}

		this.getCurrentState = function(){
			return states[currentStateIndex];
		}

		this.onStateChange = function(){};

	})

	.service('FlightService', function($http, $q){

		var getDetailPromise;
		this.getDetail = function(){
			if(getDetailPromise) return getDetailPromise;

			var defer = $q.defer();

			// $http.get(App.baseAPIUrl + '/air/id/' + encodeURIComponent(localStorage.getItem('flight')), {withCredentials: true}).then(function(response){
			$http.get(App.uploadsDir + '/flight_details.json').then(function(response){
				console.log(response.data)
				defer.resolve(response.data)
			}, function(response){
				defer.reject(response)
			})

			// defer.resolve({flight: JSON.parse(localStorage.getItem('flight')), passengers: [{type: 'ADT'}]})

			return getDetailPromise = defer.promise;
		}

		var getAirportsCodePromise;
		this.getAirportsCode = function(){
			if(getAirportsCodePromise) return getAirportsCodePromise;

			var defer = $q.defer();

			$http.get(App.uploadsDir + '/airports.json').then(function(response){
				defer.resolve(response.data);
			})

			return getAirportsCodePromise = defer.promise;
		}

		this.setInsurance = function(index){
			var insuranceOptions = [
				{value: 0},
				{value: 15},
				{value: 25},
			]

			this.getDetail().then(function(response){
				if(index !== false){
					response.insurance = [insuranceOptions[index-1]]
				} else {
					delete response.insurance;
				}


			})

			return index;
		}

		this.getSeatSelect = function(){
			return $http.get('http://www.json-generator.com/j/crchzEqUuq?indent=4');
		}

		this.book = function(){
			var defer = $q.defer();

			this.getDetail().then(function(data){
				$http.post(App.baseAPIUrl + '/air', data).then(function(pnr){
					defer.resolve(pnr)
				})
			})

			return  defer.promise;
		}
	})


//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/

angular.module('DealsCheckout.Directives', [])
	.directive('layover', function(){
		return {
			restrict: 'A',
			link: function(scope, elm, attrs){
				var from = scope.$eval(attrs['from']);
				var to = scope.$eval(attrs['to']);
				var layover = moment(to).subtract(moment(from))
				angular.element(elm).text(layover.format('HH[h]mm[min]'))
			}

		}
	})

	.directive('internalTabs', function(){
		return {
			restrict: 'C',
			link: function(scope, elm, attrs){
				App.Modules['internal-tabs'].run();
			}

		}
	})

	.directive('checkoutCreditCardInput', function(){
		return {
			restrict: 'C',
			require: 'ngModel',
			link: function(scope, elm, attrs, ctrl){
				var justNumbersReg = /^\d+$/;

				$(elm).validateCreditCard(function(result) {

					var creditCardVal = elm.val(),
						lng = creditCardVal.length;

					// is it not a number?
					if( !justNumbersReg.test(creditCardVal[lng - 1]) ) {
						elm.val( creditCardVal.substr(0, lng - 1) );
						return;
					}

					creditCardVal = creditCardVal.split(' ').join('');

					// apply spaces mask
					if (creditCardVal.length > 0) {
						creditCardVal = creditCardVal.match( new RegExp('.{1,4}', 'g') ).join(' ');
					}

					elm.val( creditCardVal );

					// get the type
					if(result.card_type !== null) {

						elm.attr('data-type-card', result.card_type.name);
						elm.removeClass('parsley-error');
						$('#parsley-id-' + elm.attr('data-parsley-id')).empty();

					} else {
						elm.attr('data-type-card', '');
						elm.addClass('parsley-error');
						$('#parsley-id-' + elm.attr('data-parsley-id')).html('<li>' + elm.attr('data-parsley-error-message') + '</li>');
					}

					scope.$apply();

				});

			}

		}
	})


//  _____ _ _ _
// |  ___(_) | |_ ___ _ __ ___
// | |_  | | | __/ _ \ '__/ __|
// |  _| | | | ||  __/ |  \__ \
// |_|   |_|_|\__\___|_|  |___/

angular.module('DealsCheckout.Filters', [])
	.filter('formatDate', function(){
		return function(data, format){
			return moment(data).format(format);
		}
	})
	.filter('passengerType', function(){
		return function(data, format){
			var types = {
				ADT: 'Adult'
			}
			return types[data];
		}
	})

