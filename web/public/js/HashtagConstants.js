//   ____                _              _
//  / ___|___  _ __  ___| |_ __ _ _ __ | |_ ___
// | |   / _ \| '_ \/ __| __/ _` | '_ \| __/ __|
// | |__| (_) | | | \__ \ || (_| | | | | |_\__ \
//  \____\___/|_| |_|___/\__\__,_|_| |_|\__|___/

angular.module('HashtagConstants', [])
	.constant('TEST', false)

	.constant('CABIN_CLASSES_CODE', {
		"F": {value: "F", text: "First Class"},
		"C": {value: "C", text: "Business"},
		"Y": {value: "Y", text: "Economy"},
		"W": {value: "W", text: "Premium Economy"}
	})
	.constant('FLIGHT_STOPS', {
		1: {text: 'Direct'},
		2: {text: '1'},
		3: {text: '2+'},
	})

	.constant('PHONE_COUNTRY_CODES', [
		{
			countryCode: "DZ",
			phoneCode: "213",
			title: "Algeria (+213)",
		},
		{
			countryCode: "AD",
			phoneCode: "376",
			title: "Andorra (+376)",
		},
		{
			countryCode: "AO",
			phoneCode: "244",
			title: "Angola (+244)",
		},
		{
			countryCode: "AI",
			phoneCode: "1264",
			title: "Anguilla (+1264)",
		},
		{
			countryCode: "AG",
			phoneCode: "1268",
			title: "Antigua &amp; Barbuda (+1268)",
		},
		{
			countryCode: "AR",
			phoneCode: "54",
			title: "Argentina (+54)",
		},
		{
			countryCode: "AM",
			phoneCode: "374",
			title: "Armenia (+374)",
		},
		{
			countryCode: "AW",
			phoneCode: "297",
			title: "Aruba (+297)",
		},
		{
			countryCode: "AU",
			phoneCode: "61",
			title: "Australia (+61)",
		},
		{
			countryCode: "AT",
			phoneCode: "43",
			title: "Austria (+43)",
		},
		{
			countryCode: "AZ",
			phoneCode: "994",
			title: "Azerbaijan (+994)",
		},
		{
			countryCode: "BS",
			phoneCode: "1242",
			title: "Bahamas (+1242)",
		},
		{
			countryCode: "BH",
			phoneCode: "973",
			title: "Bahrain (+973)",
		},
		{
			countryCode: "BD",
			phoneCode: "880",
			title: "Bangladesh (+880)",
		},
		{
			countryCode: "BB",
			phoneCode: "1246",
			title: "Barbados (+1246)",
		},
		{
			countryCode: "BY",
			phoneCode: "375",
			title: "Belarus (+375)",
		},
		{
			countryCode: "BE",
			phoneCode: "32",
			title: "Belgium (+32)",
		},
		{
			countryCode: "BZ",
			phoneCode: "501",
			title: "Belize (+501)",
		},
		{
			countryCode: "BJ",
			phoneCode: "229",
			title: "Benin (+229)",
		},
		{
			countryCode: "BM",
			phoneCode: "1441",
			title: "Bermuda (+1441)",
		},
		{
			countryCode: "BT",
			phoneCode: "975",
			title: "Bhutan (+975)",
		},
		{
			countryCode: "BO",
			phoneCode: "591",
			title: "Bolivia (+591)",
		},
		{
			countryCode: "BA",
			phoneCode: "387",
			title: "Bosnia Herzegovina (+387)",
		},
		{
			countryCode: "BW",
			phoneCode: "267",
			title: "Botswana (+267)",
		},
		{
			countryCode: "BR",
			phoneCode: "55",
			title: "Brazil (+55)",
		},
		{
			countryCode: "BN",
			phoneCode: "673",
			title: "Brunei (+673)",
		},
		{
			countryCode: "BG",
			phoneCode: "359",
			title: "Bulgaria (+359)",
		},
		{
			countryCode: "BF",
			phoneCode: "226",
			title: "Burkina Faso (+226)",
		},
		{
			countryCode: "BI",
			phoneCode: "257",
			title: "Burundi (+257)",
		},
		{
			countryCode: "KH",
			phoneCode: "855",
			title: "Cambodia (+855)",
		},
		{
			countryCode: "CM",
			phoneCode: "237",
			title: "Cameroon (+237)",
		},
		{
			countryCode: "CA",
			phoneCode: "1",
			title: "Canada (+1)",
		},
		{
			countryCode: "CV",
			phoneCode: "238",
			title: "Cape Verde Islands (+238)",
		},
		{
			countryCode: "KY",
			phoneCode: "1345",
			title: "Cayman Islands (+1345)",
		},
		{
			countryCode: "CF",
			phoneCode: "236",
			title: "Central African Republic (+236)",
		},
		{
			countryCode: "CL",
			phoneCode: "56",
			title: "Chile (+56)",
		},
		{
			countryCode: "CN",
			phoneCode: "86",
			title: "China (+86)",
		},
		{
			countryCode: "CO",
			phoneCode: "57",
			title: "Colombia (+57)",
		},
		{
			countryCode: "KM",
			phoneCode: "269",
			title: "Comoros (+269)",
		},
		{
			countryCode: "CG",
			phoneCode: "242",
			title: "Congo (+242)",
		},
		{
			countryCode: "CK",
			phoneCode: "682",
			title: "Cook Islands (+682)",
		},
		{
			countryCode: "CR",
			phoneCode: "506",
			title: "Costa Rica (+506)",
		},
		{
			countryCode: "HR",
			phoneCode: "385",
			title: "Croatia (+385)",
		},
		{
			countryCode: "CU",
			phoneCode: "53",
			title: "Cuba (+53)",
		},
		{
			countryCode: "CY",
			phoneCode: "90392",
			title: "Cyprus North (+90392)",
		},
		{
			countryCode: "CY",
			phoneCode: "357",
			title: "Cyprus South (+357)",
		},
		{
			countryCode: "CZ",
			phoneCode: "42",
			title: "Czech Republic (+42)",
		},
		{
			countryCode: "DK",
			phoneCode: "45",
			title: "Denmark (+45)",
		},
		{
			countryCode: "DJ",
			phoneCode: "253",
			title: "Djibouti (+253)",
		},
		{
			countryCode: "DM",
			phoneCode: "1809",
			title: "Dominica (+1809)",
		},
		{
			countryCode: "DO",
			phoneCode: "1809",
			title: "Dominican Republic (+1809)",
		},
		{
			countryCode: "EC",
			phoneCode: "593",
			title: "Ecuador (+593)",
		},
		{
			countryCode: "EG",
			phoneCode: "20",
			title: "Egypt (+20)",
		},
		{
			countryCode: "SV",
			phoneCode: "503",
			title: "El Salvador (+503)",
		},
		{
			countryCode: "GQ",
			phoneCode: "240",
			title: "Equatorial Guinea (+240)",
		},
		{
			countryCode: "ER",
			phoneCode: "291",
			title: "Eritrea (+291)",
		},
		{
			countryCode: "EE",
			phoneCode: "372",
			title: "Estonia (+372)",
		},
		{
			countryCode: "ET",
			phoneCode: "251",
			title: "Ethiopia (+251)",
		},
		{
			countryCode: "FK",
			phoneCode: "500",
			title: "Falkland Islands (+500)",
		},
		{
			countryCode: "FO",
			phoneCode: "298",
			title: "Faroe Islands (+298)",
		},
		{
			countryCode: "FJ",
			phoneCode: "679",
			title: "Fiji (+679)",
		},
		{
			countryCode: "FI",
			phoneCode: "358",
			title: "Finland (+358)",
		},
		{
			countryCode: "FR",
			phoneCode: "33",
			title: "France (+33)",
		},
		{
			countryCode: "GF",
			phoneCode: "594",
			title: "French Guiana (+594)",
		},
		{
			countryCode: "PF",
			phoneCode: "689",
			title: "French Polynesia (+689)",
		},
		{
			countryCode: "GA",
			phoneCode: "241",
			title: "Gabon (+241)",
		},
		{
			countryCode: "GM",
			phoneCode: "220",
			title: "Gambia (+220)",
		},
		{
			countryCode: "GE",
			phoneCode: "7880",
			title: "Georgia (+7880)",
		},
		{
			countryCode: "DE",
			phoneCode: "49",
			title: "Germany (+49)",
		},
		{
			countryCode: "GH",
			phoneCode: "233",
			title: "Ghana (+233)",
		},
		{
			countryCode: "GI",
			phoneCode: "350",
			title: "Gibraltar (+350)",
		},
		{
			countryCode: "GR",
			phoneCode: "30",
			title: "Greece (+30)",
		},
		{
			countryCode: "GL",
			phoneCode: "299",
			title: "Greenland (+299)",
		},
		{
			countryCode: "GD",
			phoneCode: "1473",
			title: "Grenada (+1473)",
		},
		{
			countryCode: "GP",
			phoneCode: "590",
			title: "Guadeloupe (+590)",
		},
		{
			countryCode: "GU",
			phoneCode: "671",
			title: "Guam (+671)",
		},
		{
			countryCode: "GT",
			phoneCode: "502",
			title: "Guatemala (+502)",
		},
		{
			countryCode: "GN",
			phoneCode: "224",
			title: "Guinea (+224)",
		},
		{
			countryCode: "GW",
			phoneCode: "245",
			title: "Guinea - Bissau (+245)",
		},
		{
			countryCode: "GY",
			phoneCode: "592",
			title: "Guyana (+592)",
		},
		{
			countryCode: "HT",
			phoneCode: "509",
			title: "Haiti (+509)",
		},
		{
			countryCode: "HN",
			phoneCode: "504",
			title: "Honduras (+504)",
		},
		{
			countryCode: "HK",
			phoneCode: "852",
			title: "Hong Kong (+852)",
		},
		{
			countryCode: "HU",
			phoneCode: "36",
			title: "Hungary (+36)",
		},
		{
			countryCode: "IS",
			phoneCode: "354",
			title: "Iceland (+354)",
		},
		{
			countryCode: "IN",
			phoneCode: "91",
			title: "India (+91)",
		},
		{
			countryCode: "ID",
			phoneCode: "62",
			title: "Indonesia (+62)",
		},
		{
			countryCode: "IR",
			phoneCode: "98",
			title: "Iran (+98)",
		},
		{
			countryCode: "IQ",
			phoneCode: "964",
			title: "Iraq (+964)",
		},
		{
			countryCode: "IE",
			phoneCode: "353",
			title: "Ireland (+353)",
		},
		{
			countryCode: "IL",
			phoneCode: "972",
			title: "Israel (+972)",
		},
		{
			countryCode: "IT",
			phoneCode: "39",
			title: "Italy (+39)",
		},
		{
			countryCode: "JM",
			phoneCode: "1876",
			title: "Jamaica (+1876)",
		},
		{
			countryCode: "JP",
			phoneCode: "81",
			title: "Japan (+81)",
		},
		{
			countryCode: "JO",
			phoneCode: "962",
			title: "Jordan (+962)",
		},
		{
			countryCode: "KZ",
			phoneCode: "7",
			title: "Kazakhstan (+7)",
		},
		{
			countryCode: "KE",
			phoneCode: "254",
			title: "Kenya (+254)",
		},
		{
			countryCode: "KI",
			phoneCode: "686",
			title: "Kiribati (+686)",
		},
		{
			countryCode: "KP",
			phoneCode: "850",
			title: "Korea North (+850)",
		},
		{
			countryCode: "KR",
			phoneCode: "82",
			title: "Korea South (+82)",
		},
		{
			countryCode: "KW",
			phoneCode: "965",
			title: "Kuwait (+965)",
		},
		{
			countryCode: "KG",
			phoneCode: "996",
			title: "Kyrgyzstan (+996)",
		},
		{
			countryCode: "LA",
			phoneCode: "856",
			title: "Laos (+856)",
		},
		{
			countryCode: "LV",
			phoneCode: "371",
			title: "Latvia (+371)",
		},
		{
			countryCode: "LB",
			phoneCode: "961",
			title: "Lebanon (+961)",
		},
		{
			countryCode: "LS",
			phoneCode: "266",
			title: "Lesotho (+266)",
		},
		{
			countryCode: "LR",
			phoneCode: "231",
			title: "Liberia (+231)",
		},
		{
			countryCode: "LY",
			phoneCode: "218",
			title: "Libya (+218)",
		},
		{
			countryCode: "LI",
			phoneCode: "417",
			title: "Liechtenstein (+417)",
		},
		{
			countryCode: "LT",
			phoneCode: "370",
			title: "Lithuania (+370)",
		},
		{
			countryCode: "LU",
			phoneCode: "352",
			title: "Luxembourg (+352)",
		},
		{
			countryCode: "MO",
			phoneCode: "853",
			title: "Macao (+853)",
		},
		{
			countryCode: "MK",
			phoneCode: "389",
			title: "Macedonia (+389)",
		},
		{
			countryCode: "MG",
			phoneCode: "261",
			title: "Madagascar (+261)",
		},
		{
			countryCode: "MW",
			phoneCode: "265",
			title: "Malawi (+265)",
		},
		{
			countryCode: "MY",
			phoneCode: "60",
			title: "Malaysia (+60)",
		},
		{
			countryCode: "MV",
			phoneCode: "960",
			title: "Maldives (+960)",
		},
		{
			countryCode: "ML",
			phoneCode: "223",
			title: "Mali (+223)",
		},
		{
			countryCode: "MT",
			phoneCode: "356",
			title: "Malta (+356)",
		},
		{
			countryCode: "MH",
			phoneCode: "692",
			title: "Marshall Islands (+692)",
		},
		{
			countryCode: "MQ",
			phoneCode: "596",
			title: "Martinique (+596)",
		},
		{
			countryCode: "MR",
			phoneCode: "222",
			title: "Mauritania (+222)",
		},
		{
			countryCode: "YT",
			phoneCode: "269",
			title: "Mayotte (+269)",
		},
		{
			countryCode: "MX",
			phoneCode: "52",
			title: "Mexico (+52)",
		},
		{
			countryCode: "FM",
			phoneCode: "691",
			title: "Micronesia (+691)",
		},
		{
			countryCode: "MD",
			phoneCode: "373",
			title: "Moldova (+373)",
		},
		{
			countryCode: "MC",
			phoneCode: "377",
			title: "Monaco (+377)",
		},
		{
			countryCode: "MN",
			phoneCode: "976",
			title: "Mongolia (+976)",
		},
		{
			countryCode: "MS",
			phoneCode: "1664",
			title: "Montserrat (+1664)",
		},
		{
			countryCode: "MA",
			phoneCode: "212",
			title: "Morocco (+212)",
		},
		{
			countryCode: "MZ",
			phoneCode: "258",
			title: "Mozambique (+258)",
		},
		{
			countryCode: "MN",
			phoneCode: "95",
			title: "Myanmar (+95)",
		},
		{
			countryCode: "NA",
			phoneCode: "264",
			title: "Namibia (+264)",
		},
		{
			countryCode: "NR",
			phoneCode: "674",
			title: "Nauru (+674)",
		},
		{
			countryCode: "NP",
			phoneCode: "977",
			title: "Nepal (+977)",
		},
		{
			countryCode: "NL",
			phoneCode: "31",
			title: "Netherlands (+31)",
		},
		{
			countryCode: "NC",
			phoneCode: "687",
			title: "New Caledonia (+687)",
		},
		{
			countryCode: "NZ",
			phoneCode: "64",
			title: "New Zealand (+64)",
		},
		{
			countryCode: "NI",
			phoneCode: "505",
			title: "Nicaragua (+505)",
		},
		{
			countryCode: "NE",
			phoneCode: "227",
			title: "Niger (+227)",
		},
		{
			countryCode: "NG",
			phoneCode: "234",
			title: "Nigeria (+234)",
		},
		{
			countryCode: "NU",
			phoneCode: "683",
			title: "Niue (+683)",
		},
		{
			countryCode: "NF",
			phoneCode: "672",
			title: "Norfolk Islands (+672)",
		},
		{
			countryCode: "NP",
			phoneCode: "670",
			title: "Northern Marianas (+670)",
		},
		{
			countryCode: "NO",
			phoneCode: "47",
			title: "Norway (+47)",
		},
		{
			countryCode: "OM",
			phoneCode: "968",
			title: "Oman (+968)",
		},
		{
			countryCode: "PW",
			phoneCode: "680",
			title: "Palau (+680)",
		},
		{
			countryCode: "PA",
			phoneCode: "507",
			title: "Panama (+507)",
		},
		{
			countryCode: "PG",
			phoneCode: "675",
			title: "Papua New Guinea (+675)",
		},
		{
			countryCode: "PY",
			phoneCode: "595",
			title: "Paraguay (+595)",
		},
		{
			countryCode: "PE",
			phoneCode: "51",
			title: "Peru (+51)",
		},
		{
			countryCode: "PH",
			phoneCode: "63",
			title: "Philippines (+63)",
		},
		{
			countryCode: "PL",
			phoneCode: "48",
			title: "Poland (+48)",
		},
		{
			countryCode: "PT",
			phoneCode: "351",
			title: "Portugal (+351)",
		},
		{
			countryCode: "PR",
			phoneCode: "1787",
			title: "Puerto Rico (+1787)",
		},
		{
			countryCode: "QA",
			phoneCode: "974",
			title: "Qatar (+974)",
		},
		{
			countryCode: "RE",
			phoneCode: "262",
			title: "Reunion (+262)",
		},
		{
			countryCode: "RO",
			phoneCode: "40",
			title: "Romania (+40)",
		},
		{
			countryCode: "RU",
			phoneCode: "7",
			title: "Russia (+7)",
		},
		{
			countryCode: "RW",
			phoneCode: "250",
			title: "Rwanda (+250)",
		},
		{
			countryCode: "SM",
			phoneCode: "378",
			title: "San Marino (+378)",
		},
		{
			countryCode: "ST",
			phoneCode: "239",
			title: "Sao Tome &amp; Principe (+239)",
		},
		{
			countryCode: "SA",
			phoneCode: "966",
			title: "Saudi Arabia (+966)",
		},
		{
			countryCode: "SN",
			phoneCode: "221",
			title: "Senegal (+221)",
		},
		{
			countryCode: "CS",
			phoneCode: "381",
			title: "Serbia (+381)",
		},
		{
			countryCode: "SC",
			phoneCode: "248",
			title: "Seychelles (+248)",
		},
		{
			countryCode: "SL",
			phoneCode: "232",
			title: "Sierra Leone (+232)",
		},
		{
			countryCode: "SG",
			phoneCode: "65",
			title: "Singapore (+65)",
		},
		{
			countryCode: "SK",
			phoneCode: "421",
			title: "Slovak Republic (+421)",
		},
		{
			countryCode: "SI",
			phoneCode: "386",
			title: "Slovenia (+386)",
		},
		{
			countryCode: "SB",
			phoneCode: "677",
			title: "Solomon Islands (+677)",
		},
		{
			countryCode: "SO",
			phoneCode: "252",
			title: "Somalia (+252)",
		},
		{
			countryCode: "ZA",
			phoneCode: "27",
			title: "South Africa (+27)",
		},
		{
			countryCode: "ES",
			phoneCode: "34",
			title: "Spain (+34)",
		},
		{
			countryCode: "LK",
			phoneCode: "94",
			title: "Sri Lanka (+94)",
		},
		{
			countryCode: "SH",
			phoneCode: "290",
			title: "St. Helena (+290)",
		},
		{
			countryCode: "KN",
			phoneCode: "1869",
			title: "St. Kitts (+1869)",
		},
		{
			countryCode: "SC",
			phoneCode: "1758",
			title: "St. Lucia (+1758)",
		},
		{
			countryCode: "SD",
			phoneCode: "249",
			title: "Sudan (+249)",
		},
		{
			countryCode: "SR",
			phoneCode: "597",
			title: "Suriname (+597)",
		},
		{
			countryCode: "SZ",
			phoneCode: "268",
			title: "Swaziland (+268)",
		},
		{
			countryCode: "SE",
			phoneCode: "46",
			title: "Sweden (+46)",
		},
		{
			countryCode: "CH",
			phoneCode: "41",
			title: "Switzerland (+41)",
		},
		{
			countryCode: "SI",
			phoneCode: "963",
			title: "Syria (+963)",
		},
		{
			countryCode: "TW",
			phoneCode: "886",
			title: "Taiwan (+886)",
		},
		{
			countryCode: "TJ",
			phoneCode: "7",
			title: "Tajikstan (+7)",
		},
		{
			countryCode: "TH",
			phoneCode: "66",
			title: "Thailand (+66)",
		},
		{
			countryCode: "TG",
			phoneCode: "228",
			title: "Togo (+228)",
		},
		{
			countryCode: "TO",
			phoneCode: "676",
			title: "Tonga (+676)",
		},
		{
			countryCode: "TT",
			phoneCode: "1868",
			title: "Trinidad &amp; Tobago (+1868)",
		},
		{
			countryCode: "TN",
			phoneCode: "216",
			title: "Tunisia (+216)",
		},
		{
			countryCode: "TR",
			phoneCode: "90",
			title: "Turkey (+90)",
		},
		{
			countryCode: "TM",
			phoneCode: "7",
			title: "Turkmenistan (+7)",
		},
		{
			countryCode: "TM",
			phoneCode: "993",
			title: "Turkmenistan (+993)",
		},
		{
			countryCode: "TC",
			phoneCode: "1649",
			title: "Turks &amp; Caicos Islands (+1649)",
		},
		{
			countryCode: "TV",
			phoneCode: "688",
			title: "Tuvalu (+688)",
		},
		{
			countryCode: "UG",
			phoneCode: "256",
			title: "Uganda (+256)",
		},
		{
			countryCode: "GB",
			phoneCode: "44",
			title: "UK (+44)",
		},
		{
			countryCode: "UA",
			phoneCode: "380",
			title: "Ukraine (+380)",
		},
		{
			countryCode: "AE",
			phoneCode: "971",
			title: "United Arab Emirates (+971)",
		},
		{
			countryCode: "UY",
			phoneCode: "598",
			title: "Uruguay (+598)",
		},
		{
			countryCode: "US",
			phoneCode: "1",
			title: "USA (+1)",
		},
		{
			countryCode: "UZ",
			phoneCode: "7",
			title: "Uzbekistan (+7)",
		},
		{
			countryCode: "VU",
			phoneCode: "678",
			title: "Vanuatu (+678)",
		},
		{
			countryCode: "VA",
			phoneCode: "379",
			title: "Vatican City (+379)",
		},
		{
			countryCode: "VE",
			phoneCode: "58",
			title: "Venezuela (+58)",
		},
		{
			countryCode: "VN",
			phoneCode: "84",
			title: "Vietnam (+84)",
		},
		{
			countryCode: "VG",
			phoneCode: "84",
			title: "Virgin Islands - British (+1284)",
		},
		{
			countryCode: "VI",
			phoneCode: "84",
			title: "Virgin Islands - US (+1340)",
		},
		{
			countryCode: "WF",
			phoneCode: "681",
			title: "Wallis &amp; Futuna (+681)",
		},
		{
			countryCode: "YE",
			phoneCode: "969",
			title: "Yemen (North)(+969)",
		},
		{
			countryCode: "YE",
			phoneCode: "967",
			title: "Yemen (South)(+967)",
		},
		{
			countryCode: "ZM",
			phoneCode: "260",
			title: "Zambia (+260)",
		},

		{
			countryCode:  "ZW",
			phoneCode: "263"	,
			title: "Zimbabwe (+263)"
		}

	])