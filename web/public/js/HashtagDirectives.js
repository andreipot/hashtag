//  ____  _               _   _
// |  _ \(_)_ __ ___  ___| |_(_)_   _____  ___
// | | | | | '__/ _ \/ __| __| \ \ / / _ \/ __|
// | |_| | | | |  __/ (__| |_| |\ V /  __/\__ \
// |____/|_|_|  \___|\___|\__|_| \_/ \___||___/
angular.module('HashtagDirectives', [])
    .directive('firstName', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: App.baseUrl + '/directive/firstName',
            scope : {
                title: '=',
                firstName: '='
            },
            controller: function($scope) {
//                if ( ! $scope.title )
//                    $scope.title = 'Mr.';
            }
        };
    });