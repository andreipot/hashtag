<?php

namespace Controllers;

require_once __DIR__.'/Payment.php';

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Flight
{
	public function index(Request $request, Application $app)
	{
		return $app['twig']->render('flight/index.twig', array(
			'menu' => 'fly',
		));
	}

	public function search(Request $request, Application $app)
	{

		$app['session']->set('search_origin', explode(',', $request->get('origin'))[0]);
		$app['session']->set('search_destination', explode(',', $request->get('destination'))[0]);
		$app['session']->set('search_depDate', explode(',', $request->get('depDate'))[0]);

		if($request->get('arvlDate')){
			$app['session']->set('search_arvlDate', explode(',', $request->get('arvlDate'))[0]);
		} else if(sizeof(explode(',', $request->get('depDate'))) > 1) {
			$app['session']->set('search_arvlDate', explode(',', $request->get('depDate'))[1]);
		}

		$app['session']->set('search_adults', $request->get('adults'));
		$app['session']->set('search_kids', $request->get('kids'));
		$app['session']->set('search_infants', $request->get('infants'));
		$app['session']->set('search_cabinClass', $request->get('cabinClass'));


		return $app['twig']->render('flight/search.twig', array(
			'menu' => 'fly',
		));
	}


	/**
	 * flight + hotel
	 */

	public function flight_search(Request $request, Application $app)
	{
		return $app['twig']->render('flight/search.twig', array(
			'menu' => 'fly',
			'together' => true
		));
	}

	public function hotel_search(Request $request, Application $app)
	{
		return $app['twig']->render('hotel/search.twig', array(
			'menu' => 'fly',
			'together' => true
		));
	}



	public function format_search(Request $request, Application $app)
	{

		$origins = $request->get('origin');
		$destinations = $request->get('destination');
		$depDates = $request->get('depDate');
		$adults = $request->get('adults');
		$kids = $request->get('kids');
		$infants = $request->get('infants');
		$cabinClass = $request->get('cabinClass');

		echo $request->get('nonstopOnly');

		$nonstopOnly = ($request->get('nonstopOnly') == 'on') ? '&nonstopOnly=true' : '&nonstopOnly=false';
		$rfdFares = ($request->get('rfdFares') == 'on') ? '&rfdFares=true' : '&rfdFares=false';

		$url = 'origin=' . implode($origins, ',') . '&destination=' . implode($destinations, ',') . '&depDate=' . implode($depDates, ',') . $nonstopOnly. $rfdFares;

		foreach($request->request as $key => $req)
		{
			if($key !== 'depDate' and $key !== 'origin' and $key !== 'destination'){
				$url .= '&' . $key . '=' . $req;
			}

		}

		return $app->redirect($app['url_generator']->generate('flight_search') . '?' . $url);

	}


	public function flex(Request $request, Application $app)
	{
		return $app['twig']->render('flight/flex.twig', array(
			'menu' => 'fly',
		));
	}

	public function options(Request $request, Application $app)
	{
		return $app['twig']->render('flight/options.twig', array(
			'menu' => 'fly',
		));
	}

	public function checkout(Request $request, Application $app)
	{

		if(!$app['session']->get('user') && !$request->get('guest')){
			$app['session']->set('login_required_checkout', 'Please login or register to book your flight.');
			$app['session']->set('login_redirect', $app['url_generator']->generate('flight_checkout'));
			return $app->redirect($app['url_generator']->generate('user_signin'));
		} else {
			$app['session']->set('login_required_checkout', null);
		}

		return $app['twig']->render('flight/checkout.twig', array(
			'menu' => 'fly',
			'datatrans' => false
		));
	}


	public function checkout_payment(Request $request, Application $app)
	{
		$data = $request->request->all();

		// print_r($data);
		// exit();

		$sign2 = Payment::sign(Payment::$key, Payment::$merchantId, $data['amount'], $data['currency'], $data['uppTransactionId']);

		if($data['status'] == 'success' && $sign2 !== $data['sign2']){
			return new Response('Fraud', 500);
		}

		return $app['twig']->render('flight/checkout.twig', array(
			'menu' => 'fly',
			'datatrans' => $data
		));
	}

}