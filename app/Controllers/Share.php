<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Share
{
	public function hotel(Request $request, Application $app)
	{
		// $request->get('id')
		return $app['twig']->render('share/hotel.twig');
	}

	public function car(Request $request, Application $app)
	{
		// $request->get('id')
		return $app['twig']->render('share/car.twig');
	}

}
