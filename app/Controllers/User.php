<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Models;
use TwitterOAuth;
use OAuthConsumer;

require_once __DIR__.'/../Classes/NameParse.php';

class User
{
	public function signin(Request $request, Application $app)
	{

		$facebook_user = $app['facebook']->getUser();

		if($request->get('code') && $facebook_user){
			$facebook_user = $app['facebook']->api('/me');

			if($app['session']->get('user')){
				$user_already_exists = Models\User::where('id', $app['session']->get('user')['id'])->first();
			} else {
				$user_already_exists = Models\User::where('email', $facebook_user['email'])->first();
			}

			if($user_already_exists){

				if(!$user_already_exists->facebook_id){
					$user_already_exists->facebook_id = $facebook_user['id'];
					$user_already_exists->facebook_token = $app['facebook']->getAccessToken();
					$user_already_exists->save();
				}

				$this->set_user_session($user_already_exists, $app);
				return $this->redirect_logged_user($app);

			} else {
				$user = new Models\User();
				$user->facebook_id = $facebook_user['id'];
				$user->facebook_token = $app['facebook']->getAccessToken();
				$user->email = $facebook_user['email'];
				$user->first_name = $facebook_user['first_name'];
				$user->last_name = $facebook_user['last_name'];
				$user->save();

				$this->set_user_session($user, $app);
				return $this->redirect_logged_user($app);
			}
		} else {
			if($app['session']->get('user')) return $this->redirect_logged_user($app);
		}

		return $app['twig']->render('user/signin.twig', array(
			'menu' => 'user',
			'facebook_user' => $facebook_user,
			'login_facebook_url' => $app['facebook']->getLoginUrl(array('scope' => 'email')),
			'modal' => ($request->get('modal')) ? $request->get('modal') : ''
		));
	}

	public function post_forgot_password(Request $request, Application $app)
	{

		if($request->get('email')){
			$email = $request->get('email');
			$user = Models\User::where('email', $email)->first();

			if($user){
				if($user->deleted){
					$app['session']->getFlashBag()->set('error_signin', "Deleted account. Please contact us.");
					return $app->redirect($app['url_generator']->generate('user_signin'));
				} else {
					$hash = md5(mt_rand() . time() . $user->id);
					$app['session']->set('reseting_email', $email);
					$user->reset_password_key = $hash;
					$user->save();

					$app->finish(function() use ($app, $hash, $user) {
						$message = \Swift_Message::newInstance()
							->setSubject('[Hashtag Travels] Reset Password')
							->setFrom(array('noreply@hashtagtravels.com'))
							->setTo(array($user->email))
							->setBody($app['twig']->render('email/reset-password.twig', array(
								'name' => $user->first_name,
								'key' => $hash
							)), 'text/html');

						$app['mailer']->send($message);

						$app['swiftmailer.spooltransport']
							->getSpool()
							->flushQueue($app['swiftmailer.transport'])
						;
					});


					return $app->redirect($app['url_generator']->generate('user_signin') . '?modal=almost-there');
				}

			} else {
				$app['session']->getFlashBag()->set('error_signin', "The e-mail $email is not registered in our database");
				return $app->redirect($app['url_generator']->generate('user_signin'));
			}
		} elseif ($request->get('password')) {
			$user = Models\User::where('email', $app['session']->get('reseting_email'))->first();
			$user->password = md5($user->salt . $request->get('password'));
			$user->save();

			$app['session']->set('reseting_email', null);
			$this->set_user_session($user, $app);
			return $app->redirect($app['url_generator']->generate('user_signin') . '?modal=successful-login');
		}

		return $app->redirect($app['url_generator']->generate('user_signin'));
	}

	public function get_forgot_password(Request $request, Application $app)
	{
		$key = $request->get('key');
		$user = Models\User::where('reset_password_key', $key)->first();

		if($user){
			$app['session']->set('reseting_email', $user->email);
			$user->reset_password_key = null;
			$user->save();
			return $app->redirect($app['url_generator']->generate('user_signin') . '?modal=create-password');
		} else {
			$app['session']->getFlashBag()->set('error_signin', "Invalid reset password key");
			return $app->redirect($app['url_generator']->generate('user_signin'));
		}

		// $user = Models\User::where('email', $email)->first();

		// if($user){
		// 	if($user->deleted){
		// 		$app['session']->getFlashBag()->set('error_signin', "Deleted account. Please contact us.");
		// 		return $app->redirect($app['url_generator']->generate('user_signin'));
		// 	} else {
		// 		$hash = md5(time() . $user->id);
		// 		$app['session']->set('reseting_email', $email);
		// 		$user->reset_password_key = $hash;
		// 		$user->save();
		// 		return $app->redirect($app['url_generator']->generate('user_signin') . '?modal=almost-there');
		// 	}

		// } else {
		// 	$app['session']->getFlashBag()->set('error_signin', "The e-mail $email is not registered in our database");
		// 	return $app->redirect($app['url_generator']->generate('user_signin'));
		// }
	}

	private function set_user_session(Models\User $user, Application $app)
	{
		$usr = $user->toArray();
		unset($usr['password']);
		unset($usr['notifications']);
		unset($usr['deleted']);
		$app['session']->set('user', $usr);
	}

	private function redirect_logged_user(Application $app)
	{
		if($app['session']->get('login_redirect')){
			return $app->redirect($app['session']->get('login_redirect'));
		} else {
			return $app->redirect($app['url_generator']->generate('user_home'));
		}
	}

	public function signin_twitter(Request $request, Application $app)
	{
		// if($_SESSION['twitter_access_token']){

		// 	$connection = new TwitterOAuth($app['config']['twitter.api_key'], $app['config']['twitter.api_secret']);
		// 	$connection->token = new OAuthConsumer($_SESSION['access_token']['oauth_token'], $_SESSION['access_token']['oauth_token_secret']);
		// 	$user_info = $connection->get('account/verify_credentials');

		// 	return $app->json($user_info);

		// }

		if($request->get('oauth_token') && $request->get('oauth_verifier')){

			$connection = new TwitterOAuth($app['config']['twitter.api_key'], $app['config']['twitter.api_secret'], $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
			$access_token = $connection->getAccessToken($request->get('oauth_verifier'));

			unset($_SESSION['oauth_token']);
			unset($_SESSION['oauth_token_secret']);

			$_SESSION['twitter_access_token'] = $access_token;

			$user_info = $connection->get('account/verify_credentials');
			$name = @parse_name($user_info->name);


			if($app['session']->get('user')) {
				$user_already_exists = Models\User::where('id', $app['session']->get('user')['id'])->first();
			} else {
				$user_already_exists = Models\User::where('twitter_id', $user_info->id)->first();
			}

			if($user_already_exists){
				$user = $user_already_exists;
				$user->twitter_id = $user_info->id;
				$user->twitter_token = $access_token['oauth_token'];
				$user->twitter_token_secret = $access_token['oauth_token_secret'];
				$user->twitter_profile_image_url = $user_info->profile_image_url;
				$user->save();
			} else {
				$user = new Models\User();
				$user->first_name = $name['first'];
				$user->last_name = @$name['last'];
				$user->twitter_id = $user_info->id;
				$user->twitter_token = $access_token['oauth_token'];
				$user->twitter_token_secret = $access_token['oauth_token_secret'];
				$user->twitter_profile_image_url = $user_info->profile_image_url;
				$user->save();
			}

			$this->set_user_session($user, $app);

			return $this->redirect_logged_user($app);

		} else {
			$connection = new TwitterOAuth($app['config']['twitter.api_key'], $app['config']['twitter.api_secret']);

			$request_token = $connection->getRequestToken('http://' . $_SERVER['SERVER_NAME'] . $app['url_generator']->generate('user_signin_twitter'));

			$_SESSION['oauth_token'] = $request_token['oauth_token'];
			$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

			$redirect_url = $connection->getAuthorizeURL($request_token['oauth_token']);

			return $app->redirect($redirect_url);
		}


	}

	public function my_account(Request $request, Application $app)
	{
		if(!$app['session']->get('user')) return $app->redirect($app['url_generator']->generate('user_signin'));

		return $app['twig']->render('user/my_account.twig', array(
			'menu' => 'user',
		));
	}

	public function bookings(Request $request, Application $app)
	{

		return $app['twig']->render('user/bookings.twig', array(
			'menu' => 'user',
		));
	}

	public function personal_details(Request $request, Application $app)
	{

		return $app['twig']->render('user/personal_details.twig', array(
			'menu' => 'user',
		));
	}

	public function get_personal_details(Request $request, Application $app)
	{
		$user = Models\User::where('id', $app['session']->get('user')['id'])->first();

		if($user){
			$user = $user->toArray();
			unset($user['password']);
			unset($user['salt']);
			return $app->json($user);
		} else {
			return $this->logout($request, $app);
		}

	}

	public function get_notifications(Request $request, Application $app)
	{
		$user = Models\User::where('id', $app['session']->get('user')['id'])->first();

		if(empty($user->notifications)){
			$user->notifications = json_encode(array(
				array(
					"name" => "Fly offers",
					"checked" => true
				),
				array(
					"name" => "Hotel offers",
					"checked" => true
				),
				array(
					"name" => "Car offers",
					"checked" => true
				),
				array(
					"name" => "Hashtag Travels news",
					"checked" => true
				)
			));

			$user->save();
		}

		return $app->json(json_decode($user->notifications));
	}

	public function put_notifications(Request $request, Application $app)
	{
		$user = Models\User::where('id', $app['session']->get('user')['id'])->first();
		$user->notifications = $request->getContent();

		if(!$user->save()){
			return $app->json(array('message' => 'Something goes wrong'), 500);
		}

		return $app->json(json_decode($request->getContent()));
	}

	public function put_personal_details(Request $request, Application $app)
	{
		// sleep(3);

		$payload = json_decode($request->getContent());


		$user_already_exists_with_email = Models\User::where('email', $payload->email)->where('id', '!=', $app['session']->get('user')['id'])->first();
		if($user_already_exists_with_email){
			return $app->json(array('message' => 'E-mail already exists'), 403);
		}

		$user = Models\User::where('id', $app['session']->get('user')['id'])->first();

		$user->title = $payload->title;
		$user->first_name = $payload->first_name;
		$user->middle_name = $payload->middle_name;
		$user->last_name = $payload->last_name;
		$user->gender = $payload->gender;
		$user->birthdate = $payload->birthdate_y . '-' . $payload->birthdate_m . '-' . $payload->birthdate_d;
		$user->email = $payload->email;
		$user->phone_code = $payload->phone_code;
		$user->phone_number = $payload->phone_number;
		$user->mobile_code = $payload->mobile_code;
		$user->mobile_number = $payload->mobile_number;
		$user->passport_country = $payload->passport_country;
		$user->passport_number = $payload->passport_number;
		$user->passport_expiration = $payload->passport_expiration_y . '-' . $payload->passport_expiration_m . '-' . $payload->passport_expiration_d;

		$user->address_country = $payload->address_country;
		$user->address_city = $payload->address_city;
		$user->address_city = $payload->address_city;
		$user->address_zipcode = $payload->address_zipcode;
		$user->address_address1 = $payload->address_address1;
		$user->address_address2 = $payload->address_address2;

		// return $app->json(array('message' => 'Something goes wrong'), 403);

		if($user->save()){
			$this->set_user_session($user, $app);
			return $app->json($payload);
		} else {
			return $app->json(array('message' => 'Something goes wrong'), 404);
		}
	}


	public function put_settings(Request $request, Application $app)
	{
		// sleep(3);

		$payload = json_decode($request->getContent());

		if($payload->new_password !== $payload->new_password_confirmation){
			return $app->json(array('message' => 'Passwords doesn`t match'), 403);
		}

		$salt = $app['session']->get('user')['salt'];
		$old_pwd_hash = md5($salt . $payload->old_password);
		$user = Models\User::where('email', $app['session']->get('user')['email'])->where('password', $old_pwd_hash)->first();

		if(!$user){
			return $app->json(array('message' => 'Invalid old password'), 403);
		}

		$user->password = md5($salt . $payload->new_password);

		if($user->save()){
			return $app->json($user);
		} else {
			return $app->json(array('message' => 'Something goes wrong'), 404);
		}
	}

	public function settings(Request $request, Application $app)
	{
		if($request->get('code')) {
			return $app->redirect($app['url_generator']->generate('user_signin') . '?code=' . $request->get('code'));
		}

		return $app['twig']->render('user/settings.twig', array(
			'menu' => 'user',
			'login_facebook_url' => $app['facebook']->getLoginUrl(array('scope' => 'email'))
		));
	}

	public function rewards(Request $request, Application $app)
	{

		return $app['twig']->render('user/rewards.twig', array(
			'menu' => 'user',
		));
	}

	public function notifications(Request $request, Application $app)
	{

		return $app['twig']->render('user/notifications.twig', array(
			'menu' => 'user',
		));
	}

	public function post_signin(Request $request, Application $app)
	{
		$email = $request->get('email');
		$password = $request->get('password');

		// $errors = $app['validator']->validateValue($email, new Assert\Email());

		//$user = Models\User::where('email', $email)->where('password', md5($password))->first();
		$user = Models\User::where('email', $email)->first();

		if($user && $user->checkPassword($password)){

			if($user->deleted == 1){
				$app['session']->getFlashBag()->set('error_signin', 'Your account were being deactivated. Please contact us.');
				return $app->redirect($app['url_generator']->generate('user_signin'));
			} else {
				$this->set_user_session($user, $app);
				return $this->redirect_logged_user($app);
			}


		} else {
			$app['session']->getFlashBag()->set('error_signin', 'User or password incorrect.');
			$app['session']->set('signin_email', $email);
			return $app->redirect($app['url_generator']->generate('user_signin'));
		}

	}

	public function post_signup(Request $request, Application $app)
	{
		$email = $request->get('email');
		$password = $request->get('password');
		$name = $request->get('name');
		$parsed_name = @parse_name($name);

		$error = false;

		if(count($app['validator']->validateValue($email, new Assert\Email())))
		{
			$error = true;
			$app['session']->getFlashBag()->set('error_signup', 'Invalid e-mail');
		}
		else if($user_already_exists = Models\User::where('email', $email)->first())
		{
			$error = true;
			$app['session']->getFlashBag()->set('error_signup', 'E-mail already registered');
		}
		else if(!isset($parsed_name['first']) or !isset($parsed_name['last']))
		{
			$error = true;
			$app['session']->getFlashBag()->set('error_signup', 'Incomplete name');
		}


		if($error){
			$app['session']->set('signup_email', $email);
			$app['session']->set('signup_password', $password);
			$app['session']->set('signup_name', $name);

			return $app->redirect($app['url_generator']->generate('user_signin'));
		} else {

			$user = new Models\User();
			$user->email = $email;

			$user->setRandomSalt();
			$user->password = md5($user->salt . $password);

			$user->first_name = $parsed_name['first'];
			$user->last_name = $parsed_name['last'];
			$user->save();

			$this->set_user_session($user, $app);
			return $this->redirect_logged_user($app);
		}

	}

	public function logout(Request $request, Application $app)
	{
		$app['session']->set('user', null);
		$app['session']->set('signin_email', null);
		$app['session']->set('signup_email', null);
		$app['session']->set('signup_password', null);
		$app['session']->set('signup_name', null);

		$app['session']->set('login_redirect', null);

		return $app->redirect($app['url_generator']->generate('home'));
	}

	public function delete_account(Request $request, Application $app)
	{
		$user = Models\User::where('id', $app['session']->get('user')['id'])->first();

		if($user) {
			$user->deleted = 1;
			$user->save();
		}

		$this->logout($request, $app);

		return new Response(201);
	}

}