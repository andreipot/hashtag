<?php

namespace Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Confirmation
{
	public function index(Request $request, Application $app)
	{
		return $app['twig']->render('confirmation/index.twig', array(
			'menu' => 'fly',
		));
	}

}