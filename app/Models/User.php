<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

class User extends Model {
	protected $table = 'front_users';
    public $timestamps = false;

    public function checkPassword($password) {
    	return md5($this->salt . $password) == $this->password;
    }

    // Generates and sets a new random salt, returns it as string
    public function setRandomSalt() {
    	$salt = '';
		for ($i = 0; $i < 8; $i++) {
            // Use a-z only, avoids escaping issues
			$salt .= chr(rand(97, 122));
		}
		$this->salt = $salt;
		return $this->salt;
    }

}
